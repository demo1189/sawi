# Movies

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.5.

## Getting Started:

Install node modules
Run `npm install` to install whole required packages

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
