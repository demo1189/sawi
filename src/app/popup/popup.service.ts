import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { PopupComponent } from './popup.component';

export interface PopupData {
  content: string;
}

@Injectable({ providedIn: 'root' })
export class PopupService {
  private readonly defaultWidth = 400;

  constructor(private dialog: MatDialog) {}

  showPopup(
    content: string,
    width: number = this.defaultWidth
  ): Observable<boolean> {
    const data: PopupData = {
      content,
    };
    const dialogRef = this.dialog.open(PopupComponent, {
      width: `${width}px`,
      disableClose: true,
      data,
    });

    return dialogRef.afterClosed();
  }
}
