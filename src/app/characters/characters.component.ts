import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Character, Characters } from './../model/character.model';
import { PopupService } from './../popup/popup.service';
import { CharactersService } from './characters.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
})
export class CharactersComponent implements OnInit {
  loading = true;
  selectedCharacter!: Character | undefined;
  charactersList: Array<Character> = [];
  characterId!: string;

  constructor(
    private charactersService: CharactersService,
    private route: ActivatedRoute,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.characterId = this.route.snapshot.params['id'];
    this.getAllCharacters();
  }

  private getAllCharacters(): void {
    this.charactersService.getAll().subscribe({
      next: (data: Characters) => {
        this.charactersList = [...data.results];
        this.checkIfLoadMore(data.next);
      },
      error: () => this.fetchError(),
    });
  }

  private getNextCharacters(page: string): void {
    this.charactersService.getCharacter(page).subscribe({
      next: (data: Characters) => {
        this.charactersList = [...this.charactersList, ...data.results];
        this.checkIfLoadMore(data.next);
      },
      error: () => this.fetchError(),
    });
  }

  private checkIfLoadMore(url: string): void {
    url
      ? this.getNextCharacters(url)
      : (this.characterId ? this.goToMovie(this.characterId) : '',
        (this.loading = false));
  }

  private goToMovie(movieId: string): void {
    this.charactersList.map((data: Character) => {
      movieId === data.url.split('/').slice(-2, -1).pop()
        ? (this.selectedCharacter = data)
        : '';
    });
  }

  private fetchError() {
    this.loading = false;
    this.popupService.showPopup(
      'Failed to load character,<br>Please try again'
    );
  }

  selectCharacter(url: string): void {
    this.charactersList.map((data: Character) => {
      url === data.url ? (this.selectedCharacter = data) : '';
    });
  }

  clearSelectedCharacter(): void {
    this.selectedCharacter = undefined;
  }

  displaySearchResult(result: Array<Character> | []): void {
    this.charactersList = result;
  }
}
