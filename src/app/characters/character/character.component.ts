import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Character } from './../../model/character.model';
import { Movie } from './../../model/movie.model';
import { MoviesService } from './../../movies/movies.service';
import { PopupService } from './../../popup/popup.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
})
export class CharacterComponent {
  movieList: Array<Movie> = [];

  @Input()
  character!: Character;

  @Output()
  backToTheList = new EventEmitter();

  constructor(
    private moviesService: MoviesService,
    private router: Router,
    private popupService: PopupService
  ) {}

  ngAfterViewInit() {
    this.getMovies();
  }

  private getMovies(): void {
    this.character.films.map((movieUrl: string) => {
      this.moviesService.getMovie(movieUrl).subscribe({
        next: (movie: Movie) => {
          this.movieList = [...this.movieList, movie];
        },
        error: () =>
          this.popupService.showPopup(
            'Failed to load movies for this character,<br>Please try again'
          ),
      });
    });
  }

  private getMovieId(url: string): string | undefined {
    return url.split('/').slice(-2, -1).pop();
  }

  goToMovie(url: string): void {
    this.router.navigate(['/movies/' + this.getMovieId(url)]);
  }

  backToTheListEvent(): void {
    this.backToTheList.emit();
  }
}
