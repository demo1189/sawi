import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { CharacterComponent } from './character/character.component';
import { CharactersComponent } from './characters.component';
import { SearchCharacterComponent } from './search-character/search-character.component';

@NgModule({
  declarations: [
    CharactersComponent,
    CharacterComponent,
    SearchCharacterComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatTableModule,
    MatListModule,
    MatInputModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
})
export class CharacterModule {}
