import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  tap,
} from 'rxjs/operators';
import { Character, Characters } from './../../model/character.model';
import { CharactersService } from './../characters.service';
@Component({
  selector: 'app-search-character',
  templateUrl: './search-character.component.html',
})
export class SearchCharacterComponent implements AfterViewInit {
  searchResults: Array<Character> = [];
  busyStatus = false;

  @ViewChild('input')
  input!: ElementRef;

  @Output() searchResult = new EventEmitter();

  constructor(private charactersService: CharactersService) {}

  ngAfterViewInit() {
    this.search();
  }

  private search() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => {
          this.busyStatus = true;
          this.searchCharacters(this.input.nativeElement.value);
        })
      )
      .subscribe();
  }

  private searchCharacters(text: string) {
    this.charactersService.search(text).subscribe({
      next: (data: Characters) => {
        this.searchResults = [...data.results];
        data.next
          ? this.getNextCharacters(data.next)
          : this.searchResultEvent();
      },
      error: () => {},
    });
  }

  private getNextCharacters(page: string) {
    this.charactersService.getCharacter(page).subscribe((data: Characters) => {
      this.searchResults = [...this.searchResults, ...data.results];
      data.next ? this.getNextCharacters(data.next) : this.searchResultEvent();
    });
  }

  private searchResultEvent() {
    this.searchResult.emit(this.searchResults);
    this.busyStatus = false;
  }
}
