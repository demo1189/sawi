import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie, Movies } from './../model/movie.model';
import { PopupService } from './../popup/popup.service';
import { MoviesService } from './movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
})
export class MoviesComponent implements OnInit {
  loading = true;
  selectedMovie!: Movie | undefined;
  movieId!: string;
  moviesList!: Array<Movie>;
  displayedColumns: string[] = ['title', 'opening_crawl'];

  constructor(
    private moviesService: MoviesService,
    private route: ActivatedRoute,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.movieId = this.route.snapshot.params['id'];
    this.getAllMovies();
  }

  private getAllMovies(): void {
    this.moviesService.getAll().subscribe({
      next: (data: Movies) => {
        this.moviesList = data.results.sort((a: Movie, b: Movie) => {
          return a.episode_id - b.episode_id;
        });
        if (this.movieId) {
          this.goToMovie(this.movieId);
        }
        this.loading = false;
      },
      error: () => this.fetchError(),
    });
  }

  private goToMovie(movieId: string): void {
    this.moviesList.map((data: Movie) => {
      movieId === data.url.split('/').slice(-2, -1).pop()
        ? (this.selectedMovie = data)
        : '';
    });
  }
  private fetchError() {
    this.loading = false;
    this.popupService.showPopup('Failed to load movies,<br>Please try again');
  }

  selectMovie(url: string): void {
    this.moviesList.map((data: Movie) => {
      url === data.url ? (this.selectedMovie = data) : '';
    });
  }

  clearSelectedMovie(): void {
    this.selectedMovie = undefined;
  }
}
