import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  private readonly baseUrl = `${environment.apiUrl}/films`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getMovie(url: string): Observable<any> {
    return this.http.get(url);
  }
}
