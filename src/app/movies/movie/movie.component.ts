import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CharactersService } from './../../characters/characters.service';
import { Character } from './../../model/character.model';
import { Movie } from './../../model/movie.model';
import { PopupService } from './../../popup/popup.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent {
  characterList: Array<Character> = [];

  @Input()
  movie!: Movie;

  @Output()
  backToTheList = new EventEmitter();

  constructor(
    private charactersService: CharactersService,
    private router: Router,
    private popupService: PopupService
  ) {}

  ngAfterViewInit() {
    this.getMovies();
  }

  private getMovies(): void {
    this.movie.characters.map((characterUrl: string) => {
      this.charactersService.getCharacter(characterUrl).subscribe({
        next: (charackter: Character) => {
          this.characterList = [...this.characterList, charackter];
        },
        error: () =>
          this.popupService.showPopup(
            'Failed to load characters for this movie,<br>Please try again'
          ),
      });
    });
  }

  private getCharacterId(url: string): string | undefined {
    return url.split('/').slice(-2, -1).pop();
  }

  goToCharacter(url: string): void {
    this.router.navigate(['/characters/' + this.getCharacterId(url)]);
  }

  backToTheListEvent(): void {
    this.backToTheList.emit();
  }
}
